## Front-End Home page

![Home page](https://www.dropbox.com/s/d3ak3gitpofl1tc/laptop_home.jpg?dl=1)

## Front-End Event page

![Event](https://www.dropbox.com/s/iru9th3rrjgtz6w/laptop_event.png?dl=1)

## Front-End View page

![View](https://www.dropbox.com/s/fpz3307q6oc7t83/laptop_view.jpg?dl=1)

