<?php

namespace App\Services\Convoe;

class DistanceInHourService
{
    public $status = 1;

    public function calcInHour($distance)
    {
        if ($this->status === 1) {
            if ($distance > 800 && $distance < 1200) {
                $hour = 1;
            } elseif ($distance > 1200 && $distance < 2000) {
                $hour = 2;
            } elseif ($distance > 2200 && $distance < 3500) {
                $hour = 2.5;
            } elseif ($distance > 3500 && $distance < 4000) {
                $hour = 3;
            } elseif ($distance > 4000 && $distance < 6000) {
                $hour = 3.5;
            } else {
                $hour = 0;
            }

            return $hour;
        } else {
            return '--';
        }
    }
}
