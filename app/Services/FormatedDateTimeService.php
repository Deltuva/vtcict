<?php

namespace App\Services;

class FormatedDateTimeService
{
    public function todayHmFormat($dateTime)
    {
        return __('vtcict.date_today_hm', [
            'hm' => $dateTime->format('H:i')
        ]);
    }

    public function startEndFormat($start, $end)
    {
        return $start->format('H:i') . ' - ' . $end->format('H:i') . '<i class="far fa-clock ml-2"></i>';
    }

    public function start($start)
    {
        return $start->format('H:i');
    }

    public function finish($end)
    {
        return $end->format('H:i');
    }
}
