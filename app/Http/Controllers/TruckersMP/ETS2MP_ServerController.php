<?php

namespace App\Http\Controllers\TruckersMP;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class ETS2MP_ServerController extends Controller
{
    public function getData()
    {
        $url = 'https://api.truckersmp.com/v2/servers';
        $client = new Client(['header' => ['User-Agent' => 'PAPJAUTAS-TRUCKERS']]);

        $response = $client->request('GET', $url);

        if ($response->getStatusCode() == 200) {
            $tmpDataCollection = [];
            $tmpItemsArray = json_decode((string)$response->getBody(), true);

            $itemsResponse = $tmpItemsArray['response'];

            if (is_array($itemsResponse) || is_object($itemsResponse)) {
                foreach ($itemsResponse as $key => $collectionItem) {
                    if (!is_null($collectionItem)) {
                        $tmpDataCollection[$key] = $collectionItem;
                    }
                }
            } else {
                $tmpDataCollection = [];
            }

            $statusCode = false;

            return response()->json([
                'error' => $statusCode,
                'response' => $tmpDataCollection
            ]);
        } else {
            $error = true;

            return response()
        ->json([
            'error' => $error,
            'message' => 'Servers data empty.'
        ]);
        }
    }
}
