<?php

namespace App\Http\Controllers\Convoe;

use App\ConvoeEvent;
use App\Http\Controllers\Controller;

class ConvoeEventViewController extends Controller
{
    public function show($id)
    {
        try {
            $convoe = new ConvoeEvent();
            $event = $convoe->where('id', $id)->firstOrFail();

            return view(
                'convoes.view',
                compact(
                    'event'
                )
            );
        } catch (\Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }
}
