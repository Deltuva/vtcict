<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CrawlerEts2CompaniesController extends Controller
{
    public function crawlerAction() 
    {
        $game = 'ETS2';
        $url = 'https://truck-simulator.fandom.com/wiki/Bergen';
        $crawler = \Goutte::request('GET', $url);
        $head_title = $crawler
            ->filter('h2.pi-title')
            ->text();
        echo $head_title."<br/>";

        $items = $crawler
            ->filter('[data-source=companies]')
            ->each(function ($node) { 
            return $node
                ->filter('ul > li')
                ->each(function ($node2) {
                
                return $node2->text();
            });
        });

        dd($items);
    }
}
