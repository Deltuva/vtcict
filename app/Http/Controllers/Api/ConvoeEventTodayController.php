<?php

namespace App\Http\Controllers\Api;

use App\ConvoeEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConvoeEventTodayController extends Controller
{
    /**
     * Count All Convoe Events Today
     * @param  ConvoeEvent $convoeEventModel
     * @return json|array
     */
    public function getData(Request $request, ConvoeEvent $convoeEventModel)
    {
        $columns = [
            'convoe_events.id AS id',
        ];

        try {
            $eventsToday = $convoeEventModel
                ->select($columns)
                ->whereRaw('DATE(created_at) = CURDATE()')
                ->count('created_at');
        } catch (Exception $e) {
            $statusCode = '404';
        } finally {
            return response()
                ->json(['counter' => $eventsToday, 'status' => 'success']);
        }
    }
}
