<?php

namespace App\Http\Controllers\Api;

use App\ConvoeEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConvoeEventHistoryController extends Controller
{
    /**
     * Get Convoe Events Today
     * @param  ConvoeEvent $convoeEventModel
     * @return json|array
     */
    public function getData(Request $request, ConvoeEvent $convoeEventModel)
    {
        $columns = [
            'convoe_events.id AS id',
            'convoe_events.game AS game',
            'convoe_events.map_photo AS map_photo',
            'convoe_events.trailer_photo AS trailer_photo',
            'convoe_events.truck_photo AS truck_photo',
            'convoe_events.load_from AS load_from',
            'convoe_events.load_to AS load_to',
            'convoe_events.company_from AS company_from',
            'convoe_events.company_to AS company_to',
            'convoe_events.cargo AS cargo',
            'convoe_events.timeout_zone AS timeout_zone',
            'convoe_events.discord AS discord',
            'convoe_events.start_dues AS start_dues',
            'convoe_events.end_dues AS end_dues',
            'convoe_events.distance AS distance',
            'convoe_events.user_id AS user_id',
            'convoe_events.is_published AS is_published',
            'convoe_events.created_at AS created_at',
            'convoe_events.updated_at AS updated_at',
        ];

        try {
            $events = $convoeEventModel
              ->select($columns);
            $events->orderBy('id', 'asc');
            $result = $events->paginate((int) $request->get('limit', 3));
        } catch (Exception $e) {
            $statusCode = '404';
        } finally {
            return response()
                ->json($result)
                ->withCallback($request->callback);
        }
    }
}
