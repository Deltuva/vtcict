<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show Home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Steam User Logout
     *
     * @return void
     */
    public function logout()
    {
        Auth::logout();

        return redirect()->route('home');
    }
}
