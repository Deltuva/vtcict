<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationCompany extends Model
{
    protected $table = 'location_companies';
}
