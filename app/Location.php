<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'locations';

    protected $fillable = [
        'game', 'city_name', 'country', 'depots', 'features', 'expansion'
    ];
}
