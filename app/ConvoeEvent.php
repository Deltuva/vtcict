<?php

namespace App;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class ConvoeEvent extends Model
{
    const DIFF_IN_DAYS = 1;
    const DIFF_IN_MINUTES = 5;
    const ETS2_TYPE = 'ETS';
    const ATS_TYPE = 'ATS';

    protected $table = 'convoe_events';

    protected $dates = ['start_dues', 'end_dues'];

    public function getTodayDateStatus()
    {
        $t = new Carbon($this->created_at, 'Europe/Vilnius');

        $todayStatus = ($t->diff(Carbon::now())->days < self::DIFF_IN_DAYS) ? true : false;

        return $todayStatus;
    }

    /**
     * Get Game type name
     */
    public function getGameType()
    {
        if ($this->game == self::ETS2_TYPE) {
            return __('vtcict.convoes.ets_game_type_name', [
                'vtc' => config('app.name')
            ]);
        } else {
            return __('vtcict.convoes.ats_game_type_name', [
                'vtc' => config('app.name')
            ]);
        }
    }

    /**
     * Get Event Indent Id
     */
    public function getEventKey()
    {
        return __('vtcict.convoes.game_key_indent_name', [
            'event_id' => $this->id
        ]);
    }
}
