/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';
import './libs/fontawesome_v5';

/**
 * Dropdown Hover
 */
import './modules/dropdown/dropdown';

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});

/**
 * Components
 */
Vue.component('convoes-event-popup-modal', require('./components/convoes/partials/popups/PopupEventModalComponent.vue'));
Vue.component('etsmp-servers-component', require('./components/ETSMPServersComponent.vue'));
Vue.component('atsmp-servers-component', require('./components/ATSMPServersComponent.vue'));
Vue.component('convoes-event-component', require('./components/convoes/ConvoesEventComponent.vue'));
Vue.component('convoes-event-history-component', require('./components/convoes/ConvoesEventHistoryComponent.vue'));
Vue.component('convoes-event-today-counter-component', require('./components/convoes/ConvoesEventTodayCounterComponent.vue'));
Vue.component('convoes-event-today-search-component', require('./components/convoes/ConvoesEventTodaySearchComponent.vue'));

const app = new Vue({
  el: '#app'
});
