$(function() {
    $("#s-comment-form").hide();

    $(".o-comment-form button").click(function(e) {
        e.preventDefault();

        if ($("#s-comment-form").is(":visible")) {
            $("#s-comment-form").hide(200);
        } else {
            $("#s-comment-form").show(200);
            $("html, body").animate(
                {
                    scrollTop: $("#s-comment-form").offset().top
                },
                500
            );
        }
    });
});
