/**
 * API
 */

class Api {
    static getServersData(then) {
        return axios(config.AXIOS_ETS2MP_SERVERS_POINT)
            .then(response => then(response.data.response))
            .catch();
    }
}

export default Api;
