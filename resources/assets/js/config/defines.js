/**
 * Defines constants
 */
export const __VTC = "VTCICT";
export const __VTC_API_DOMAIN = "http://localhost:8000/";
export const AXIOS_ETS2MP_SERVERS_POINT = "/api/ets2mp_servers/";