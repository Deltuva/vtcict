@extends('layouts.app')

@section('content')

<section class="event">
  <div class="event-head">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          @if (!is_null(config('app.name')))
            <h1>{{ $event->getGameType() }}</h1>
          @endif

          <h2>{{ $event->getEventKey() }}</h2>
        </div>
      </div>
    </div>
  </div>

  <div class="event-content">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">

          <div class="event-content--stats">
            <div class="picture">
              <a class="event-picture--img" href="#">
                <img class="img-fluid" src="http://localhost:8000/assets/img/slider/demo.png" alt="....">
              </a>
            </div>
            @inject('statistic', 'App\Services\Convoe\StatisticDataService')

            <div class="row bg-opt text-center py-3">
              {{-- @if ($event->views) --}}
                <div class="col-6 col-lg-3 col-sm-3">
                  <i class="far fa-eye mr-1"></i> 0
                </div>
              {{-- @endif --}}

              {{-- @if ($statistic->getCommentsTotal()) --}}
                <div class="col-6 col-lg-3 col-sm-3">
                  <i class="fas fa-comments mr-1"></i>
                  {{ $statistic->getCommentsTotal() }}
                </div>
              {{-- @endif --}}
              <div class="col-6 col-lg-3 col-sm-3">
                <i class="fas fa-thumbs-up mr-1"></i> 0
              </div>
              <div class="col-6 col-lg-3 col-sm-3">
                <i class="fas fa-thumbs-down mr-1"></i> 0
              </div>
            </div>

            @inject('datetime', 'App\Services\FormatedDateTimeService')

            {{-- <div class="today">
              {{ $datetime->todayHmFormat($event->start_dues) }}
            </div> --}}

            @if ($event->load_from || $event->load_to)
              <div class="event-content--info">
                <div class="row text-center">
                  <div class="col-lg-4">
                    <div class="convoy">
                      <div class="convoy-start">
                        {{ __('vtcict.convoes.drive_from_company') }}
                      </div>
                      <div class="convoy-finish">
                        {{ $event->load_from }}
                      </div>
                      <div class="convoy-company">
                        {{ $event->company_from }}
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="convoy">
                      @if ($event->start_dues || $event->end_dues)
                        <div class="convoy-start">
                          <i class="fas fa-arrows-alt-h"></i>
                          <div class="hm">
                            {!! $datetime->startEndFormat($event->start_dues, $event->end_dues) !!}
                          </div>
                        </div>
                      @endif

                      @if ($event->distance > 0)
                        <div class="convoy-km">
                          {{ __('vtcict.convoes.distance', [
                            'distance' => $event->distance
                          ]) }}
                        </div>
                      @endif

                      @if ($event->distance)
                        @inject('distance', 'App\Services\Convoe\DistanceInHourService')

                        <div class="convoy-company">
                          {{ __('vtcict.convoes.distance_in_hour', [
                            'hour' => $distance->calcInHour($event->distance)
                          ]) }}.
                        </div>
                      @endif
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="convoy">
                      <div class="convoy-start">
                        {{ __('vtcict.convoes.drive_in_company') }}
                      </div>
                      <div class="convoy-finish">
                        {{ $event->load_to }}
                      </div>
                      <div class="convoy-company">
                        {{ $event->company_to }}
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="event-content--server">
                <div class="row pb-4">
                  <div class="col-sm-12 server-bl">
                    <div class="server-bl--name">
                      {{ __('vtcict.convoes.drive_server') }}: {{ $event->server }}{{ $event->server_type }}
                    </div>
                  </div>

                  @if ($event->start_dues || $event->end_dues)
                    <div class="col-sm-12 server-bl">
                      <div class="server-bl--name">
                        {{ __('vtcict.convoes.drive_from') }}: {{ $datetime->start($event->start_dues) }}
                      </div>
                    </div>
                    <div class="col-sm-12 server-bl">
                      <div class="server-bl--name">
                        {{ __('vtcict.convoes.drive_in') }}: {{ $datetime->finish($event->end_dues) }}
                      </div>
                    </div>
                  @endif

                  @if (!is_null($event->timeout_zone))
                    <div class="col-sm-12 server-bl">
                      <div class="server-bl--name">
                        {{ __('vtcict.convoes.load_in_afk') }}: {{ $event->timeout_zone }}
                      </div>
                    </div>
                  @endif

                  @if (!is_null($event->discord))
                    <div class="col-sm-12 server-bl">
                      <div class="server-bl--name">
                        {{ __('vtcict.convoes.comc') }}: {{ $event->discord }}
                      </div>
                    </div>
                  @endif
                </div>

                @if (!is_null($event->additional_information_member))
                  <div class="desc-info">
                    <div class="col-sm-12 pl-0 pr-0">
                      <h3><i class="fas fa-info fa-2x mr-2"></i>
                        {{ __('vtcict.convoes.info_for_member_head') }}
                      </h3>
                      <div class="picture">
                        <a class="event-picture--img" href="#">
                          <img class="img-fluid" src="http://localhost:8000/assets/img/slider/demo.png" alt="....">
                        </a>
                      </div>
                    </div>
                  </div>
                @endif

                @if (!is_null($event->additional_information_guest))
                  <div class="desc-info">
                    <div class="col-sm-12 pl-0 pr-0">
                      <h3><i class="fas fa-info fa-2x mr-2"></i>
                        {{ __('vtcict.convoes.info_for_guest_head') }}
                      </h3>
                      <div class="picture">
                        <a class="event-picture--img" href="#">
                          <img class="img-fluid" src="http://localhost:8000/assets/img/slider/demo.png" alt="....">
                        </a>
                      </div>
                    </div>
                  </div>
                @endif
              </div>
            @endif
          </div>
        </div>

        @include('convoes.partials.history')
      </div>
    </div>
  </div>
</section>
@endsection