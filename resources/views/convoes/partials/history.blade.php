<convoes-event-history-component :load="'{!! __('vtcict.convoes.load') !!}'"
  :load_more="'{!! __('vtcict.convoes.load_more') !!}'"
  :status_loading="'{!! __('vtcict.convoes.status_loading') !!}'"
  :status_empty="'{!! __('vtcict.convoes.status_empty') !!}'">
</convoes-event-history-component>