<header class="main-head">
  <div class="container">
    <div class="navbar-container">

      <div class="topbar">
        <ul class="topbar-nav justify-content-end">
          @guest
          <li><a href="auth/steam"><i class="fab fa-steam-square"></i> {{ __('vtcict.topbar.sign_steam') }}</a></li>
          @else
          <li><a href="/video-reports"><span>{{ __('vtcict.topbar.members') }}:</span> 1</a></li>
          <li><span class="label label-default" data-toggle="tooltip" data-placement="bottom" title="">
              <a href="/profile" style="color: white;">Shopass</a></span>
          </li>
          <li><a href="#">{{ __('vtcict.topbar.logout') }}</a></li>
          @endguest
        </ul>
      </div>
      <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="#">
          {{-- <img src="{{ asset('assets/img/lvr.png') }}" class="rtmplogo" alt="ReportsTMP Logo"> --}}
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsiblenavbar">
          <i class="fas fa-bars fa-w-14 fa-2x"></i>
        </button>

        <div class="collapse navbar-collapse justify-content-end" id="collapsiblenavbar">
          <ul class="navbar-nav">

            <li class="nav-item">
              <a class="nav-link" data-toggle="modal" data-target="#convoe-modal">{{ __('vtcict.navbar.questions') }}</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                {{ __('vtcict.navbar.downloads') }}
              </a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="#">ETS 2</a>
                <a class="dropdown-item" href="#">ATS 2</a>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="https://forum.truckersmp.com/">{{ __('vtcict.navbar.forum') }}</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">{{ __('vtcict.navbar.support') }}</a>
            </li>
          </ul>
      </nav>
    </div>
  </div>

  {{-- @include('partials.topline') --}}
</header>