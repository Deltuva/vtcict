<footer class="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <nav class="foot-nav my-5">
                    <div class="row">
                        <div class="col-sm-12 no-padding-right text-center">
                            (c) Development
                            <strong><a href="http://papjautas.com">Papjautas</a></strong>
                            <p>Virtual Trucking Company Own Website</p>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</footer>
