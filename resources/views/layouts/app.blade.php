<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" type="image/png" href="{{ asset('assets/icons/favicon-16x16.png') }}">
  <link href="https://fonts.googleapis.com/css?family=Exo+2:100,300,400,600,700|Roboto&amp;subset=cyrillic,latin-ext"
    rel="stylesheet">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name') }}</title>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <!--[if lt IE 9]>
  <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
  <![endif]-->

  <meta name="application-name" content="ReportsTSMP" />
  <meta name="msapplication-TileColor" content="#ff0000" />
  <link rel="apple-touch-icon" href="{{ asset('assets/icons/apple-touch-icon.png') }}">
  <link rel="apple-touch-icon" sizes="32x32" href="{{ asset('assets/icons/favicon-32x32.png') }}">
  <link rel="apple-touch-icon" sizes="16x16" href="{{ asset('assets/icons/favicon-16x16.png') }}">

</head>

<body>


  <div id="app">
    <!-- Top Header nav -->
    @include('includes.header')

    @yield('content')

    <!-- Bottom footer -->
    @include('includes.footer')
  </div>

  <!-- Scripts -->
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script src="{{ mix('js/app.js') }}"></script>

  @stack('scripts')
</body>

</html>