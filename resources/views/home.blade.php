@extends('layouts.app')
@section('content')

<main role="main">

  <!--START OF SLIDER SECTION-->
  <div id="carouselLceIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselLceIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselLceIndicators" data-slide-to="1"></li>
      <li data-target="#carouselLceIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <!-- Slide #1 - Set the background image for this slide in the line below -->
      <div class="carousel-item active" style="background-image: url('{{ asset('assets/img/slider/demo.png') }}')">
        {{--
                <div class="carousel-caption d-none d-md-block">
                    <h3><a href="#">VTC LCE - Lavr Crazy Express</a></h3>
                    <h3><a href="#">Банда дольнобоев</a></h3>
                </div> --}}
      </div>
      <!-- Slide #2 - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('{{ asset('assets/img/slider/2.png') }}')"></div>
      <!-- Slide #3 - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('{{ asset('assets/img/slider/demo2.png') }}')"></div>
    </div>
    <a class="carousel-control-prev" href="#carouselLceIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only"></span>
    </a>
    <a class="carousel-control-next" href="#carouselLceIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only"></span>
    </a>
  </div>
  <!--END OF SLIDER SECTION-->

  <!--START OF SERVERS SECTION-->
  <section id="servers-section">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-6 col-md-12 col-sm-12">
          <h2 class="py-4">{{ __('vtcict.servers.heading') }} <span>ETS2MP</span></h2>
          <div class="servers">
            <etsmp-servers-component :playing_server="'{!! __('vtcict.servers.playing_server') !!}'"
              :status_online="'{!! __('vtcict.servers.status_online') !!}'"
              :status_offline="'{!! __('vtcict.servers.status_online') !!}'"
              :status_loading="'{!! __('vtcict.servers.status_loading') !!}'"
              :status_favourited="'{!! __('vtcict.servers.status_favourited') !!}'">
            </etsmp-servers-component>
          </div>
        </div>
        <div class="col-12 col-lg-6 col-md-12 col-sm-12">
          <h2 class="py-4">{{ __('vtcict.servers.heading') }} <span>ATSMP</span></h2>
          <div class="servers">
            <atsmp-servers-component :playing_server="'{!! __('vtcict.servers.playing_server') !!}'"
              :status_online="'{!! __('vtcict.servers.status_online') !!}'"
              :status_offline="'{!! __('vtcict.servers.status_online') !!}'"
              :status_loading="'{!! __('vtcict.servers.status_loading') !!}'"
              :status_favourited="'{!! __('vtcict.servers.status_favourited') !!}'">
            </atsmp-servers-component>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--END OF SERVERS SECTION-->

  {{--
    <div class="e">
        <iframe src="https://ets2map.com?follow=2559009" height="300" width="1000px"></iframe>
    </div>
    --}}

  <!--START OF CONVOYS POPUP MODAL -->
  <convoes-event-popup-modal :convoes_head="'{!! __('vtcict.convoes.today') !!}'"
    :convoes_all="'{!! __('vtcict.convoes.today_all') !!}'" :convoes_empty="'{!! __('vtcict.convoes.today_empty') !!}'"
    :drive_from_company="'{!! __('vtcict.convoes.drive_from_company') !!}'"
    :drive_in_company="'{!! __('vtcict.convoes.drive_in_company') !!}'" :cargo="'{!! __('vtcict.convoes.cargo') !!}'">
  </convoes-event-popup-modal>
  <!--END OF CONVOYS POPUP MODAL -->

  <!--START OF CONVOYS SECTION-->
  <convoes-event-component :server_head="'{!! __('vtcict.convoes.heading') !!}'"
    :drive_from="'{!! __('vtcict.convoes.drive_from') !!}'" :drive_in="'{!! __('vtcict.convoes.drive_in') !!}'"
    :load_from="'{!! __('vtcict.convoes.load_from') !!}'" :load_in="'{!! __('vtcict.convoes.load_in') !!}'"
    :cargo="'{!! __('vtcict.convoes.cargo') !!}'" :load_in_afk="'{!! __('vtcict.convoes.load_in_afk') !!}'"
    :button="'{!! __('vtcict.convoes.button') !!}'" :load="'{!! __('vtcict.convoes.load') !!}'"
    :load_more="'{!! __('vtcict.convoes.load_more') !!}'"
    :status_loading="'{!! __('vtcict.convoes.status_loading') !!}'"
    :status_empty="'{!! __('vtcict.convoes.status_empty') !!}'"></convoes-event-component>
  <!--END OF CONVOYS SECTION-->

  <!--START OF CONVOYS SECTION-->
  <section id="members-section">
    <div class="container">
      <h2 class="py-4">{{ __('vtcict.members.heading') }} <span>{{ config('app.name') }}</span></h2>

      <div class="row">
        <div class="col-sm-12">
          <nav class="members scrollMembers">
            <ul class="ul-members clearfix">
              <li>
                <a href="#">
                  <img src="{{ asset('assets/img/lvr.png') }}" class="lvr img-fluid" alt="member#{{ config('app.name') }}"> [{{ config('app.name') }}]Shopass
                </a>
                <span class="online-status">{{ __('vtcict.members.player_in_game_status') }}</span>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </section>
  <!--END OF CONVOYS SECTION-->
</main>
@endsection