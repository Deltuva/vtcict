<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('auth/steam', 'SteamAuthController@redirectToSteam')->name('auth.steam');
Route::get('auth/steam/handle', 'SteamAuthController@handle')->name('auth.steam.handle');
Route::get('convoy/{id}', 'Convoe\ConvoeEventViewController@show')->name('convoe.event.view');

/*
|--------------------------------------------------------------------------
| ETS2 API
|--------------------------------------------------------------------------
*/
Route::get('api/ets2mp_player/{tmpid}', 'ETS2MP_PlayerController@getData');
Route::get('api/ets2mp_tracker/{tmpid}', 'ETS2MP_TrackerController@getData');
Route::get('api/ets2mp_servers', 'TruckersMP\ETS2MP_ServerController@getData');
Route::get('api/ets2mp_ban/{tmpid}', 'ETS2MP_BanController@getData');
Route::get('api/convoe_events', 'Api\ConvoeEventController@getData')->name('api.convoe.events');
Route::get('api/convoe_events_history', 'Api\ConvoeEventHistoryController@getData')->name('api.convoe.events.history');
Route::get('api/convoe_events_today', 'Api\ConvoeEventTodayController@getData')->name('api.convoe.events.today');

Route::get('api/crawler_cities', 'Data\CrawlerEts2CitiesController@crawlerAction')->name('crawler.cities');
Route::get('api/crawler_companies', 'Data\CrawlerEts2CompaniesController@crawlerAction')->name('crawler.companies');
Route::get('auth/logout', 'HomeController@logout')->name('auth.logout');
