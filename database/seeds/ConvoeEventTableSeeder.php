<?php

use Carbon\Carbon;
use App\ConvoeEvent;
use Illuminate\Database\Seeder;

class ConvoeEventTableSeeder extends Seeder
{
    const CONVOYS_LIMIT = 10;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $startDate1 = Carbon::today()->addHours(18);
        $endDate1 = $startDate1->addMinutes(30);

        $startDate2 = Carbon::today()->addHours(19);
        $endDate2 = $startDate2->addMinutes(0);

        factory(ConvoeEvent::class, self::CONVOYS_LIMIT)
            ->create([
                'start_dues' => $startDate1,
                'end_dues' => $endDate2,
            ]);
    }
}
