<?php

use App\Cargo;
use Illuminate\Database\Seeder;

class CargoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $url = 'http://vtcict.eu/cargo_data.txt';
        $contents = file_get_contents($url);

        $dataResponse = explode("\n", $contents);

        if (is_array($dataResponse) || is_object($dataResponse)) 
        {
            foreach($dataResponse as $lineItem) {
                if (!is_null($lineItem)) {
                    $lineItemExplode = explode("=", $lineItem);

                    if (!is_null($lineItemExplode)) {
                        // Insert cargo items values
                        Cargo::create([
                            'cargo' => $lineItemExplode[0],
                            'label_cargo' => $lineItemExplode[1]
                        ]);
                    }
                }
            }
        }
    }
}
