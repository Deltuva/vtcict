<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ConvoeEventTableSeeder::class);
        $this->call(CargoTableSeeder::class);
        $this->call(LocationTableSeeder::class);
    }
}
