<?php

use App\Location;
use Illuminate\Database\Seeder;

class LocationTableSeeder extends Seeder
{
    protected $city;
    protected $country;
    protected $depots;
    protected $features;
    protected $expansion;
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $game = 'ETS2';
        $url = 'https://truck-simulator.fandom.com/wiki/List_of_Cities_in_Euro_Truck_Simulator_2';
        $crawler = \Goutte::request('GET', $url);
        $items = $crawler->filter('table.article-table')->filter('tr')->each(function ($tr, $i) { 
            return $tr->filter('td')->each(function ($td, $i) {
                if ($i == 3) {
                    return '---';
                } else {
                    return trim($td->text());
                }
            });
        });

        $keys = array_keys($items);

        for($i = 1; $i < count($items); $i++) {
            //echo $keys[$i] . "{<br>";

            foreach($items[$keys[$i]] as $key => $value) 
            {
                if ($key == 0) {
                    $this->city = $value;
                    //echo "City Name: ".$this->city . "<br>";
                } else if ($key == 1) {
                    $this->country = $value;
                    //echo "Country: ".$this->country . "<br>";
                } else if ($key == 2) {
                    $this->depots = $value;
                    //echo "Depots: ".$this->depots . "<br>";
                } else if ($key == 3) {
                    $this->features = $value;
                    //echo "Features: ".$this->features . "<br>";
                } else if ($key == 4) {
                    $this->expansion = $value;
                    //echo "Expansion: ".$this->expansion . "<br>";
                }
            }
            Location::create([
                'game'         => $game,
                'city'    => $this->city,
                'country'      => $this->country,
                'depots'       => $this->depots,
                'features'     => $this->features,
                'expansion'    => $this->expansion
            ]);
            //echo "}<br>";
        }
    }
}
