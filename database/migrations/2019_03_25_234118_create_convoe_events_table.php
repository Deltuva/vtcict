<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvoeEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convoe_events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('game')->nullable();
            $table->string('map_photo')->nullable();
            $table->string('trailer_photo')->nullable();
            $table->string('truck_photo')->nullable();
            $table->integer('views')->default(0);
            $table->integer('like')->default(0);
            $table->integer('dislike')->default(0);
            $table->string('load_from')->nullable();
            $table->string('load_to')->nullable();
            $table->string('company_from')->nullable();
            $table->string('company_to')->nullable();
            $table->string('cargo')->nullable();
            $table->string('timeout_zone')->nullable();
            $table->string('server')->nullable();
            $table->integer('server_type')->default(2);
            $table->string('discord')->nullable();
            $table->longText('additional_information_member')->nullable();
            $table->longText('additional_information_guest')->nullable();
            $table->longText('additional_information')->nullable();
            $table->dateTime('start_dues')->nullable();
            $table->dateTime('end_dues')->nullable();
            $table->integer('distance')->default(0);
            $table->integer('user_id')->unsigned()->nullable();
            $table->boolean('is_published')->default(false);
            $table->timestamps();
            // $table->softDeletes();

            // $table->index(['deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convoe_events');
    }
}
