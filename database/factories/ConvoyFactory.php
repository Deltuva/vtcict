<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\ConvoeEvent::class, function (Faker $faker) {
    return [
        'game' => 'ETS',
        'map_photo' => '-',
        'trailer_photo' => '-',
        'truck_photo' => '-',
        'load_from' => 'Vilnius',
        'load_to' => 'Kaunas',
        'company_from' => 'GNT',
        'company_to' => 'BLT',
        'cargo' => 'Apples',
        'timeout_zone' => 'Utena',
        'server' => 'EU',
        'server_type' => '2',
        'discord' => '-',
        'additional_information_member' => 'More info for members now empty.',
        'additional_information_guest' => 'More info for guests now empty.',
        'additional_information' => 'More info now empty.',
        'distance' => $faker->numberBetween(300, 6000),
        'user_id' => '1',
        'is_published' => true
    ];
});
